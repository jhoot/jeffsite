import React, { Component } from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';

import styles from './Feed.scss';
import Project from '../../components/Project/Project';

class Feed extends Component {
    state = {
        loading: true,
        projects: []
    }
    componentDidMount() {
        // axios.post('https://personalsite-jeff.firebaseio.com/projects.json', {
        //         name: 'Cannabest Medical',
        //         image: 'https://i.imgur.com/UfnDCZF.png',
        //         description: 'Native Android App for Cannabest, a company that is working to improve the quality of medical marijuana treatment through personalized data.',
        //         tech: 'android java'
        // }).then(res => {
        //     console.log(res);
        // }).catch(err => {
        //     console.log(err);
        // });
        const projects = [];
        axios.get('https://personalsite-jeff.firebaseio.com/projects.json')
            .then(res => {
                console.log(res.data);
                for (let proj in res.data) {
                    projects.push(res.data[proj]);
                }
                this.setState({
                    projects: projects,
                    loading: false
                });
                console.log('[inside response] ', this.state.projects);
            }).catch(err => {
                console.log(err);
                this.setState({
                    loading: false
                });
            });
    }
    render () {
        let body = <Spinner />;
        if (!this.state.loading) {
            let finalArray = this.state.projects.map(proj => {
                console.log(proj.name);
                return <Project key={proj.name} name={proj.name} description={proj.description} tech={proj.tech} image={proj.image} />;
            });
            body = (
                <div>
                    {finalArray}
                </div>
            );
        }
        return (
            <div className={styles.feed}>
                {body}
            </div>
        );
    }
}

export default Feed;