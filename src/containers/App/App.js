import React, { Component } from 'react';

import styles from './App.scss';
import Header from '../../components/UI/Header/Header';
import Feed from '../Feed/Feed';
import Modal from '../../components/UI/Modal/Modal';
import Toolkit from '../../components/Icons/Toolkit/Toolkit';
import Connect from '../../components/Icons/Connect/Connect';
import Contact from '../../components/Icons/Connect/Contact';
import MobileHeader from '../../components/UI/MobileHeader/MobileHeader';

class App extends Component {
  state = {
    showModal: false,
    toolkit: false,
    connect: false,
    contact: false
  }
  
  modalClosedHandler = () => {
    this.setState({showModal: false});
  }

  toolkitHandler = () => {
    this.setState({
      toolkit: true,
      connect: false,
      contact: false,
      showModal: true
    });
  }
  connectHandler = () => {
    this.setState({
      toolkit: false,
      connect: true,
      contact: false,
      showModal: true
    });
  }
  contactHandler = () => {
    this.setState({
      toolkit: false,
      connect: false,
      contact: true,
      showModal: true
    });
  }

  render() {

    let modalContents = <p>Loading</p>;

    if (this.state.toolkit) {
      modalContents = (
        <Toolkit />
      );
    } else if (this.state.connect) {
      modalContents = (
        <Connect />
      );
    } else if (this.state.contact) {
      modalContents = (
        <Contact />
      );
    }

    return (
      <div className={styles.app}>
        <Modal show={this.state.showModal} modalClosed={this.modalClosedHandler}>
          {modalContents}
        </Modal>
        <MobileHeader />
        <Header toolkit={this.toolkitHandler} connect={this.connectHandler} contact={this.contactHandler}/>
        <Feed />
      </div>
    );
  }
}

export default App;
