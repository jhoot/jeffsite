import React from 'react';

import styles from './TopHeader.scss';

const topHeader = (props) => (
    <div className={styles.topHeader}>
        <img src="https://i.imgur.com/7hke301.jpg" alt="header profile" />
        <h1>Jeff Hooton</h1>
        <h4>Hi my name is Jeff and as you've probably guessed, I'm a developer. I am passionate about technology and solving problems. I currently live in South Florida and spend my free time coding, at the beach or on my porch reading.</h4>
        <h4>I am currently open for side projects, so make sure you reach out to me through any of the channels listed below!</h4>
    </div>
);

export default topHeader;