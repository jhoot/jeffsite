import React from 'react';

import styles from './Header.scss';
import TopHeader from '../TopHeader/TopHeader';
import BottomHeader from '../BottomHeader/BottomHeader';

const header = (props) => {
    return (
        <div className={styles.header}>
            <div className={styles.overlay}>
                
            </div>
            <TopHeader />
            <BottomHeader toolkit={() => props.toolkit()} connect={() => props.connect()} contact={() => props.contact()}/>
        </div>
    );
};

export default header;