import React from 'react';

import styles from './Spinner.scss';

const spinner = (props) => (
    <div className={styles.loader}>Loading...</div>
);

export default spinner;