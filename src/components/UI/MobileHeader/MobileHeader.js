import React from 'react';

import styles from './MobileHeader.scss';

const mobileHeader = (props) => (
    <div className={styles.mobileHeader}>
        MOBILE HEADER
    </div>
);

export default mobileHeader;