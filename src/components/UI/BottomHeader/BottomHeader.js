import React from 'react';

import styles from './BottomHeader.scss';
import Connect from '../../Icons/Connect/Connect'
import Contact from '../../Icons/Connect/Contact';
import Toolkit from '../../Icons/Toolkit/Toolkit';

const bottomHeader = (props) => (
    <div className={styles.bottomHeader}>
        <div className={styles.buttonContainer}>
            <button onClick={props.toolkit}>Technology</button>
            <button onClick={props.connect}>Connect</button>
            <button onClick={props.contact}>Contact</button>
        </div>
        <div className={styles.componentContainer}>
            <Toolkit className={styles.Toolkit}/>
            <Connect className={styles.Connect}/>
            <Contact className={styles.Contact}/>
        </div>
    </div>
);

export default bottomHeader;