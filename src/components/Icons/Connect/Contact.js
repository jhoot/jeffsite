import React from 'react';

import styles from './Connect.scss';

const contact = (props) => (
    <div className={styles.contact}>
        <h2>Reach Out To Me</h2>
        <a href="mailto:jeffreydhooton@gmail.com"><i className="fas fa-envelope-square"></i></a>
        <a href="tel:5615413310"><i className="fas fa-phone-square"></i></a>
    </div>
);

export default contact;