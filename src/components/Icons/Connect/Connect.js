import React from 'react';

import styles from './Connect.scss';

const connect = (props) => (
    <div className={styles.contact}>
        <h2>Find Me On</h2>
        <a href="https://gitlab.com/jhoot" rel="noopener noreferrer" target="_blank"><i className="fab fa-gitlab"></i></a>
        <a href="https://instagram.com/jeffdhooton" rel="noopener noreferrer" target="_blank"><i className="fab fa-instagram"></i></a>
        <a href="https://www.linkedin.com/in/jeff-hooton-4038183a/" rel="noopener noreferrer" target="_blank"><i className="fab fa-linkedin"></i></a>
        
    </div>
);

export default connect;