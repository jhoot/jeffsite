import React from 'react';

import styles from './Toolkit.scss';

const toolkit = (props) => (
    <div className={styles.technology}>
        <h2>My Tech Toolkit</h2>
        <i className="fab fa-html5"></i>
        <i className="fab fa-css3-alt"></i>
        <i className="fab fa-sass"></i>
        <i className="fab fa-php"></i>
        <i className="fab fa-wordpress"></i>
        <i className="fab fa-react"></i>
        <i className="fab fa-node"></i>
        <i className="fab fa-python"></i>
        <i className="fab fa-android"></i>
        <i className="fab fa-java"></i>
    </div>
);

export default toolkit;