import React from 'react';

import styles from './Project.scss';

const project = (props) => {
    let techUsed = props.tech.split(' ');
    let objects = techUsed.map(tech => {
        let classes = "fab fa-" + tech;
        return <i className={classes}></i>;
    })
    return (
        <div className={styles.project}>
        <div className={styles.top}>
            <h1>{props.name}</h1>
                {objects}
            <p>{props.description}</p>
        </div>
        <div className={styles.bottom}>
            <img src={props.image} alt="project" />
        </div>
    </div>
    );
}

export default project;